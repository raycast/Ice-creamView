﻿using System.Collections;
using System.Collections.Generic;
using IcecreamView;
using UnityEngine;
using UnityEngine.UI;

public class viewmodule_helpPanel : IC_AbstractModule
{

	[BindUIPath("BG/ListTest" , "Text ({n})")]
	private List<Text> listTexts;

	[BindUIPath("BG/ListTest" , "Text ({n})/Button")]
	private List<Button> testButtons;
	
	[BindUIPath("BG/ListTest/Text (0)")]
	private Text test01;

	public override void OnInitView()
	{
	}

	[BindUIEvent(1)]
	public void Test(EventArg rEventArg)
	{
		Debug.Log("测试Event: " + rEventArg.GetValue<string>(0));
		foreach (var Text in this.listTexts)
		{
			Debug.Log(Text.name);
		}
	}
}
